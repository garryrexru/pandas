import pandas as pd
import numpy as np

# Версия pandas==1.3.0

# 1. Объединить все таблицы в одну.
# DataFrame каждой таблицы
first_table = pd.read_excel('./task/1/1_sp500_1.xlsx')
second_table = pd.read_excel('./task/1/1_sp500_2.xlsx')
third_table = pd.read_excel('./task/1/1_sp500_3.xlsx')
fourth_table = pd.read_excel('./task/1/1_sp500_4.xlsx')
fifth_table = pd.read_excel('./task/1/1_sp500_5.xlsx')


# Список DataFrames всех таблиц.
tables_df_list = [first_table, second_table, third_table, fourth_table, fifth_table]

# Объединение DataFrames всех таблиц.
appended_df = pd.concat(tables_df_list)

# Сохранение итогового DataFrame в таблицу Excel.
appended_df.to_excel('./result/1_sp500.xlsx', index=False)


# 2. "Смержить" таблицу из прошлого шага с таблицей 2_sp500.
# DataFrame таблицы 2_sp500
table_for_merge = pd.read_excel('./task/2/2_sp500.xlsx')

merged_df = pd.merge(appended_df, table_for_merge, on='Symbol')

# Удаление лишних пробелов в конце строки столбца Sector.
for index, value in enumerate(merged_df['Sector']):
    merged_df.loc[index, 'Sector'] = value.rstrip()

# Сохранение итогового DataFrame в таблицу Excel.
merged_df.to_excel('./result/merged_table.xlsx', index=False)


# 3. Создать сводную таблицу. Средний прайс по секторам.
pivot_table = pd.pivot_table(merged_df, index='Sector', values='Price', aggfunc=np.mean)

# Сохранение итогового DataFrame в таблицу Excel.
pivot_table.to_excel('./result/pivot_table.xlsx')

# Сохранение итогового DataFrame в csv.
pivot_table.to_csv('./result/pivot.csv', sep='|', encoding='Windows-1251')

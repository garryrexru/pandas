-- 1 Выгрузить сотрудников, у кого оклад больше, чем у своего руководителя

SELECT
  A.ID,
  A.Name
FROM
  Employe A,
  Employe B
WHERE
  A.Salary > B.Salary
  AND B.ID = A.SHIEF_ID;

-- 2 Выгрузить ФИО сотрудников, их оклад и средний оклад по их подразделению.

SELECT
  Name,
  Salary,
  (
    SELECT
      ROUND(
        Avg(Salary)
      ) AS avg_salary
    FROM
      Employe AS C
    WHERE
      C.DEP_ID = B.DEP_ID
    GROUP BY
      C.DEP_ID
  )
FROM
  Employe AS B;

-- 3 Выгрузить сотрудников, у кого наибольший оклад в своем подразделении

SELECT
  A.ID,
  A.Name
FROM
  Employe A
WHERE
  A.Salary = (
    SELECT
      max(Salary)
    FROM
      Employe B
    WHERE
      B.DEP_ID = A.DEP_ID
  );

-- 4 Выгрузить ID подразделения, кол-во сотрудников, в которых не более 10

SELECT
  DEP_ID
FROM
  Employe
GROUP BY
  DEP_ID
HAVING
  COUNT(*) < 10;

-- 5 Выгрузить список сотрудников, у которых не установлен руководитель подразделения, в котором работает сотрудник.

SELECT
  A.Name
FROM
  Employe AS A
  LEFT JOIN Employe AS B ON (
    A.SHIEF_ID = B.ID
    AND A.DEP_ID = B.DEP_ID
  )
WHERE
  B.ID IS NULL;

-- 6 Выгрузить список ID подразделений с минимальной суммарной зарплатой сотрудников

WITH dep_salary AS (
  SELECT
    DEP_ID,
    sum(Salary) AS salary
  FROM
    Employe
  GROUP BY
    DEP_ID
)
SELECT
  DEP_ID
FROM
  dep_salary
WHERE
  dep_salary.salary = (
    SELECT
      min(salary)
    FROM
      dep_salary
  );
